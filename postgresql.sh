#!/bin/sh
set -e

case $1 in
  stop)
    /bin/su postgres -c "pg_ctl stop -D ${PGDATA}" ;;
  reload)
    /bin/su postgres -c "pg_ctl reload -D ${PGDATA}" ;;
  *)
    pid=/tmp/postgresql.pid
    user=postgres
    log=${PGDATA}/postgresql.log

    install -dm 2750 -o postgres -g postgres /run/postgresql
    chown -R postgres:postgres ${PGBASE}

    if test ! -f ${PGDATA}/PG_VERSION ; then
      su - postgres -c "/usr/bin/initdb --locale ${LANG} -E UTF8 -D ${PGDATA}"
      su - postgres -c "/usr/bin/pg_ctl start --pgdata ${PGDATA}"
      su - postgres -c "/usr/bin/createuser --login ${PGUSER}"
      su - postgres -c "/usr/bin/createdb --owner ${PGUSER} ${PGDB}"
      su - postgres -c "/usr/bin/pg_ctl stop --pgdata ${PGDATA}"

      echo "host ${PGDB} ${PGUSER} ${PGCLIENT} trust" >> ${PGDATA}/pg_hba.conf
      echo "host ${PGDB}_test ${PGUSER} ${PGCLIENT} trust" >> ${PGDATA}/pg_hba.conf

      echo "listen_addresses = '*'" >> ${PGDATA}/postgresql.conf
      echo "external_pid_file = '${pid}'" >> ${PGDATA}/postgresql.conf
    fi

    rm -f ${pid}
    daemonize -u ${user} -c ${PGDATA} -o ${log} -a -e ${log} \
              /usr/bin/postgres

    su - postgres -c "echo 'create extension if not exists pgcrypto;' | psql -U postgres"
    ;;
esac
