FROM sutty/monit:latest
MAINTAINER "f <f@sutty.nl>"

ENV PGBASE /var/lib/postgresql
ENV PGDATA $PGBASE/13/data
ENV LANG en_US.utf8
ENV PGUSER sutty
ENV PGDB sutty
ENV PGVER 13
ENV PGCLIENT sutty
ENV PAGER "less -niSFX"

RUN apk add --no-cache postgresql postgresql-contrib daemonize less

COPY ./postgresql.sh /usr/local/bin/postgresql
COPY ./monit.conf /etc/monit.d/postgresql.conf

RUN chmod 750 /usr/local/bin/postgresql

EXPOSE 5432
VOLUME $PGBASE
